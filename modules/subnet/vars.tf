variable "vpc_name" {
  type = string
  default = "vpc-devops-telkomsel-9"
}

variable "auto_subnet" {
  default = false
}

variable "subnet_name" {
  type = string
  default = "subnet-devops9"
}

variable "cidr_range" {
  type = string
  default = "10.2.0.0/16"
}

variable "region" {
  type = string
  default = "us-west2"
}

