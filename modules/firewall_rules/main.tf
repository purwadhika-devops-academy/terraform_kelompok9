resource "google_compute_firewall" "firewall-rules" {
  name    = var.fw_name
  network = var.vpc_name

  allow {
    protocol = var.protocol_name
    ports    = var.port
  }

  target_tags = var.target_tag
    
}
