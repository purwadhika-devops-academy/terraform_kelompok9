variable "fw_name" {
  type = string
  default = "fw-ssh"
}

variable "vpc_name" {
  type = string
  default = "vpc-devops-telkomsel-9"
}

variable "protocol_name" {
  type = string
  default = "tcp"
}

variable "port" {
  type = list(string)
  default = [
    "22"
  ]
}

variable "target_tag" {
  type = list(string)
  default = [
    "ssh-server"
  ]
}
