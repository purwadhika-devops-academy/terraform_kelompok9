resource "google_container_cluster" "primary" {
  name               = var.kube_name
  location           = var.kube_location
  initial_node_count = var.init_node

  ip_allocation_policy {
    cluster_ipv4_cidr_block = ""
    services_ipv4_cidr_block = ""
  }
  network = var.vpc_name
  subnetwork = var.subnet_name

  private_cluster_config {
    enable_private_endpoint = false
    enable_private_nodes = true
    master_ipv4_cidr_block = var.master_ipv4_cidr_block
  }
  
}
