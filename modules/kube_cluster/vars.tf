variable "kube_name" {
  type = string
  default = "devops-telkomsel9"
}

variable "kube_location" {
  type = string
  default = "us-west2-a"
}

variable "init_node" {
  type = number
  default = 1
}

variable "vpc_name" {
  type = string
  default = "vpc-devops-telkomsel-9"
}

variable "subnet_name" {
  type = string
  default = "subnet-devops9"
}

variable "master_ipv4_cidr_block" {
  type = string
  default = "172.16.0.16/28"
}


#VARIABLE FOR NAMESPACE
variable "namespace_name" {
  type = list(string)
  default = ["application","tools"]
}



