variable "vpc_name" {
  type = string
  default = "vpc-devops-telkomsel-9"
}

variable "auto_subnet" {
  default = false
}

variable "region" {
  type = string
  default = "us-west2"
}

