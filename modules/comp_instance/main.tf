resource "google_compute_instance" "appserver" {
    name = var.instance_name
    zone = var.zone
    machine_type = var.machine_type
    tags = var.tags_name
    boot_disk {
      initialize_params {
          image = var.instance_image
      }
    }
    network_interface {
      network = var.vpc_name
      subnetwork = var.subnet_name

      access_config {
        
      }
    }
}