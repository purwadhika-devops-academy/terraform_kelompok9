variable "instance_name" {
    type = string
    default = "jenkins-group9"
  
}

variable "zone" {
  type = string
  default = "us-west2-a"
}

variable "machine_type" {
    type = string
    default = "e2-small"
  
}

variable "instance_image" {
    type = string
    default = "debian-cloud/debian-9"
  
}

variable "vpc_name" {
    type = string
    default = "vpc-devops-telkomsel-9"
  
}
variable "tags_name" {
    type = list(string)
    default = [ "jenkins", "tools" ]
  
}

variable "subnet_name" {
  type = string
  default = "private-subnet-devops9"
}