resource "kubernetes_namespace" "namespace" {
  for_each = toset(var.ns_name)
  metadata {
    name = each.value
  }
}