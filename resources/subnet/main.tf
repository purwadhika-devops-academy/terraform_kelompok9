terraform {
  backend "gcs" {
    bucket  = "tf-state-devops9"
    prefix  = "terraform/subnet"
    credentials = "../../group-9-322208-ab2e0dc1b1f7.json"
  }
}


provider "google" {
  credentials = file("../../group-9-322208-ab2e0dc1b1f7.json")
  project     = "group-9-322208"
  region      = "us-west2"
  zone        = "us-west2-a"
}


module "private-subnet" {
  source  = "../../modules/subnet"
  
  subnet_name = "private-subnet-devops9"
  cidr_range  = "10.2.0.0/16"
}

module "public-subnet" {
  source  = "../../modules/subnet"

  subnet_name = "public-subnet-devops9"
  cidr_range  = "10.3.0.0/16"
}


