terraform {
  backend "gcs" {
    bucket  = "tf-state-devops9"
    prefix  = "terraform/vpc"
    credentials = "./group-9-322208-ab2e0dc1b1f7.json"
  }
}

provider "google" {
  credentials = file("./group-9-322208-ab2e0dc1b1f7.json")
  project     = "group-9-322208"
  region      = "us-west2"
  zone        = "us-west2-a"
}


module "vpc" {
  source  = "./modules/vpc"

}

