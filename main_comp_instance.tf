terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "3.78.0"
    }
  }
  backend "gcs" {
    bucket  = "tf-state-devops9"
    prefix  = "terraform/comp_instance"
    credentials = "./group-9-322208-ab2e0dc1b1f7.json"
  }
}

provider "google" {
  credentials = file("./group-9-322208-ab2e0dc1b1f7.json")
  project     = "group-9-322208"
  region      = "us-west2"
  zone        = "us-west2-a"
}


module "comp_instance_jenkins" {
  source  = "./modules/comp_instance"

}
