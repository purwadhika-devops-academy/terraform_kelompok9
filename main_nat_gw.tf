terraform {
  backend "gcs" {
    bucket  = "tf-state-devops9"
    prefix  = "terraform/nat_gw"
    credentials = "./group-9-322208-ab2e0dc1b1f7.json"
  }
}


provider "google" {
  credentials = file("./group-9-322208-ab2e0dc1b1f7.json")
  project     = "group-9-322208"
  region      = "us-west2"
  zone        = "us-west2-a"
}

resource "google_compute_router" "router" {
  name    = "router-devops9"
  network = "vpc-devops-telkomsel-9"
  bgp {
    asn            = 64514
    advertise_mode = "CUSTOM"
  }
}

resource "google_compute_router_nat" "nat" {
  name                               = "nat-gw-devops9"
  router                             = google_compute_router.router.name 
  region                             = "us-west2"
  nat_ip_allocate_option             = "AUTO_ONLY"
  source_subnetwork_ip_ranges_to_nat = "LIST_OF_SUBNETWORKS"

  subnetwork{
    name                    = "private-subnet-devops9"
    source_ip_ranges_to_nat = ["ALL_IP_RANGES"]
  }
}

