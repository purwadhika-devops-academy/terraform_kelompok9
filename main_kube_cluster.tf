terraform {
  backend "gcs" {
    bucket  = "tf-state-devops9"
    prefix  = "terraform/kubernetes_cluster"
    credentials = "./group-9-322208-ab2e0dc1b1f7.json"
  }
}


provider "google" {
  credentials = file("./group-9-322208-ab2e0dc1b1f7.json")
  project     = "group-9-322208"
  region      = "us-west2"
  zone        = "us-west2-a"
}

# provider "kubernetes" {
#   load_config_file = "false"
# }

module "kube_cluster" {
  source  = "./modules/kube_cluster"
  
  init_node = 2
  vpc_name = "vpc-devops-telkomsel-9"
  subnet_name = "private-subnet-devops9"
}

