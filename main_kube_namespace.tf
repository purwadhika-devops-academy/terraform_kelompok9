terraform {
  required_providers {
    kubernetes = {
      source = "hashicorp/kubernetes"
      version = "2.4.1"
    }
  }
  backend "gcs" {
    bucket  = "tf-state-devops9"
    prefix  = "terraform/kube_namespace"
    credentials = "./group-9-322208-ab2e0dc1b1f7.json"
  }
}

provider "kubernetes" {
  config_path = "~/.kube/config"
}

module "namespace" {
  source  = "./modules/kube_namespace"
}
