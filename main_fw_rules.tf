terraform {
  backend "gcs" {
    bucket  = "tf-state-devops9"
    prefix  = "terraform/firewall_rules"
    credentials = "./group-9-322208-ab2e0dc1b1f7.json"
  }
}

provider "google" {
  credentials = file("./group-9-322208-ab2e0dc1b1f7.json")
  project     = "group-9-322208"
  region      = "us-west2"
  zone        = "us-west2-a"
}


module "fw-rules-http" {
  source  = "./modules/firewall_rules"

  fw_name = "devops9-allow-http"
  protocol_name = "tcp"
  port  = ["80"]
  target_tag = ["to-do"]
}

module "fw-rules-https" {
  source  = "./modules/firewall_rules"

  fw_name = "devops9-allow-https"
  protocol_name = "tcp"
  port  = ["443"]
  target_tag = ["to-do"]
}

module "fw-rules-grafana" {
  source  = "./modules/firewall_rules"

  fw_name = "devops9-allow-grafana"
  protocol_name = "tcp"
  port  = ["3000"]
  target_tag = ["grafana"]
}

module "fw-rules-jenkins" {
  source  = "./modules/firewall_rules"

  fw_name = "devops9-allow-jenkins"
  protocol_name = "tcp"
  port  = ["8080","8443"]
  target_tag = ["jenkins"]
}

module "fw-rules-prometheus" {
  source  = "./modules/firewall_rules"

  fw_name = "devops9-allow-prometheus"
  protocol_name = "tcp"
  port  = ["9090"]
  target_tag = ["prometheus"]
}


#UNTUK PROVISIONING DAN TROUBLESHOT SAJA OPEN PORT 22
module "fw-rules-allowssh" {
  source  = "./modules/firewall_rules"

  fw_name = "devops9-allow-allowssh"
  protocol_name = "tcp"
  port  = ["22"]
  target_tag = ["tools"]
}
